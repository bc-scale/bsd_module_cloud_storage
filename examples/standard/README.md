# Standard Example

This example illustrates how to use the `cloud_storage` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gcs\_location | The location of the bucket | `string` | `"australia-southeast1"` | no |
| labels | labels to be applied | `any` | n/a | yes |
| project\_id | The project in which the buckets will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| common\_bucket\_names | The names of the common bucket made |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
