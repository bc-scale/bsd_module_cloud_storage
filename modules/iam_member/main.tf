variable "bucket_name" {
  description = "The name of the bucket we will create the permission on"

}
variable "role" {
  description = "value"
  default     = "roles/storage.objectViewer"
  # roles/storage.objectAdmin = writer
  # roles/storage.objectViewer = reader

}
variable "member" {
  description = "The group we apply the permission to"

}

resource "google_storage_bucket_iam_member" "member" {
  bucket = var.bucket_name
  role   = var.role
  member = var.member
}
