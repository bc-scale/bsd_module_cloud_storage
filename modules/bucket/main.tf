variable "project_id" {
  description = "The project in which the buckets will be created"
}

variable "gcs_location" {
  description = "The location of the bucket"
  default     = "australia-southeast1"

}

variable "bucket_name" {
  description = "The name of the bucket to be made"
}

variable "force_destroy" {
  description = "If the objects should be destroyed"

}
variable "storage_class" {
  description = "The storage class for the bucket"
  default     = "STANDARD"
}

variable "labels" {
  description = "labels to be applied"
  default     = {}
}

variable "public" {
  default = false

}
resource "google_storage_bucket" "bucket" {
  name          = var.bucket_name
  project       = var.project_id
  location      = var.gcs_location
  force_destroy = var.force_destroy
  storage_class = var.storage_class
  labels        = var.labels
}

module "public_permission" {
  count  = var.public == true ? 1 : 0
  source = "../iam_member"
  depends_on = [
    google_storage_bucket.bucket
  ]
  bucket_name = google_storage_bucket.bucket.name
  member      = "allUsers"

}
