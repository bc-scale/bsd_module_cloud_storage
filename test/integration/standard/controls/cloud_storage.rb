title 'Verifying the cloud storage has been set up correctly'

PROJECT_ID = attribute('project_id', description: 'The project the buckets are created in')
GCS_LOCATION = attribute('gcs_location', description: 'The default location of gcs storage')

control "common-buckets" do
 title 'Ensure the common buckets have been created and versioning is disabled'
 [PROJECT_ID + '-data-storage', PROJECT_ID + '-code-storage'].each do |bucket|
    describe google_storage_bucket(name: bucket) do
      it { should exist }
      its('storage_class') { should eq "STANDARD" }
      its('location') { should eq GCS_LOCATION }
      its('versioning.enabled') { should eq nil}
    end
  end
end
